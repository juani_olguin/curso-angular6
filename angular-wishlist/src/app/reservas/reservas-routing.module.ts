import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioLogueadoGuard } from '../guards/usuario-logueado/usuario-logueado.guard';
import { ReservasDetalleComponent } from './reservas-detalle/reservas-detalle.component';
import { ReservasListadoComponent } from './reservas-listado/reservas-listado.component';

const routes: Routes = [
  { path: 'reservas', component: ReservasListadoComponent, canActivate: [UsuarioLogueadoGuard] },
  { path: 'reservas/:id', component: ReservasDetalleComponent, canActivate: [UsuarioLogueadoGuard] }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservasRoutingModule { }
