import { HtmlTagDefinition } from '@angular/compiler';
import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';



@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})

export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongNombre = 3;
  searchResults: string[];
  //forwardRef referencias circulares
  constructor(private fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) { 
    //inicializar
    this.onItemAdded = new EventEmitter();
    //vinculacion con tag html
    this.fg = this.fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidatorParametrizable(this.minLongNombre)
      ])],
      url: ['', Validators.compose([
        Validators.required
      ])]
    });
    
    //observador de tipeo
    this.fg.valueChanges.subscribe((form: any) =>{
      console.log('cambio el formulario: ', form);
    })
  }

  ngOnInit(): void {
    const elemNombre = <HTMLInputElement>document.getElementById('nombre');
    //suscribirnos a cuando se apreta una tecla
    fromEvent(elemNombre, 'input')
    //pipe es para operaciones en serie
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value), //nos interesa la cadena entera luego de apretar una tecla
        filter(text => text.length > 2),//si se cumple condicion, sigue el pipe
        debounceTime(200), //dar tiempo entre verificaciones del evento de teclado
        distinctUntilChanged(), //avanza cuando sea distinta la cadena que analiza respecto a la anterior
        switchMap((text: string) => ajax(this.config.apiEndPoint + '/ciudades?q=' + text))
        //ajax es ASINCRONICO
      ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
  }

  guardar(nombre: string, url: string): boolean {
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): { [s: string]: boolean }{
    const l = control.value.toString().trim().length;//trim junta espacios en blanco
    if(l>0 && l<4){
      return {invalidNombre: true}
    }
    return null;
  }
  
  nombreValidatorParametrizable(minLong: number): ValidatorFn{
    return(control: FormControl): { [s: string]: boolean} | null => {
      const l = control.value.toString().trim().length;//trim junta espacios en blanco
    if(l>0 && l<minLong){
      return {minLongNombre: true};
    }
    return null;
    }
  }

}