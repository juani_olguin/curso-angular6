
export class DestinoViaje {
    
    selected: boolean;
    servicios: string[];
    id: String;
    public votes = 0;

    constructor(public nombre: string,public u: string){
        this.servicios = ["Pileta", "Desayuno"];
    }
    isSelected():boolean{
        return this.selected;
    }
    setSelected(s: boolean){
        this.selected = s;
    }
    voteUp() {
        this.votes++;
    }
    voteDown() {
        if(this.votes > 0){
            this.votes--;
        }
    }
}