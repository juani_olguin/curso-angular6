import { Directive, OnDestroy, OnInit } from '@angular/core';

@Directive({
  selector: '[appEspiame]'
})
export class EspiameDirective implements OnInit, OnDestroy{
  static nextId = 0;//se comparte entre todas las instancias
  log = (msg: string) => console.log(`Evento #${EspiameDirective.nextId++} ${msg}`);
  ngOnInit(){ this.log(`#########******** onInit`); }
  ngOnDestroy(){ this.log(`#########******** onDestroy`); }
}
//utilizar ` ` nos permite la interpolacion de cadenas
//`${codigo}`