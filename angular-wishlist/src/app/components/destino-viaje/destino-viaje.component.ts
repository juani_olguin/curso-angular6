import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { VoteDownAction, VoteUpAction } from '../../models/destinos-viajes-state.model';
import { DestinoViaje as dv} from './../../models/destino-viaje.model';
import { trigger, state, style, transition, animate } from '@angular/animations';
//trigger -> darle nombre y agrupar logica de una animacion
//state -> para una animacion se necesitan al menos dos estados
//transition -> transicion entre dos estados

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'Black',
        color: 'White',
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke',
        color: 'Black',
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('1.2s', style({
          backgroundColor: 'Black',
          color: 'White',
          transform: 'rotateY(360deg)'
        }))
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('0.5s')
      ]),
      
    ])
  ]
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: dv;
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass = 'col-md-6 col-sm-12 col-lg-4';
  @Output() onClicked: EventEmitter<dv>;

  constructor(private store: Store<AppState>) {
    this.onClicked = new EventEmitter();
  }
   
  ngOnInit(): void {
  }

  ir(){
    //clicked tiene que ser un atributo que nos deje disparar eventos
    this.onClicked.emit(this.destino);
    return false;
  }
  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }
  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
}
