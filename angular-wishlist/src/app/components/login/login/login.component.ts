import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  mensajeError: string

  constructor(
    public authService: AuthService, 
    private router: Router,
    private route: ActivatedRoute) { 
    this.mensajeError = '';
  }

  ngOnInit(): void {
  }
//login sincronico
  login(username: string, password: string): boolean{
    this.mensajeError = "";
    if(!this.authService.login(username,password)){
      this.mensajeError = "Usuario o contraseña incorrectos";
      setTimeout(function(){
        this.mensajeError = '';
      }.bind(this),2500);
    }
    if(this.authService.login(username, password)){
      this.router.navigateByUrl('/home');
    }
    return false;
  }

  logout(): boolean{
    this.authService.logout();
    return false;
  }
}
