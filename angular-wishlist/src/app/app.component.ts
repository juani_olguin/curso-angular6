import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  
  
})

export class AppComponent {
  title = 'Wishlist';
  time = new Observable(observer => {
    setInterval(() => observer.next(new Date().toString()), 1000);
    
  }); 
  isLogged = new AuthService;

  constructor(public translate: TranslateService){
    console.log('**************** get translation');
    translate.getTranslation('en').subscribe(x => console.log('x: '+ JSON.stringify(x)));
    translate.setDefaultLang('es');
  }

  agregar(titulo:HTMLInputElement){
    console.log(titulo.value);
  }
  estaLogged():boolean{
    return this.isLogged.getUser();
  }
  
}

